<?php

/**
 * Extension Manager/Repository config file for ext "sp_initial".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'SP initial',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4.0-12.4.99',
            'fluid_styled_content' => '12.4.0-12.4.99',
            'rte_ckeditor' => '12.4.0-12.4.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'TeamXat\\SpInitial\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Karin Gerbrich',
    'author_email' => 'karin@gerbrich.at',
    'author_company' => 'Team-X.at',
    'version' => '1.0.0',
];
